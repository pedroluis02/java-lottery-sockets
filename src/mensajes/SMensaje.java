
package mensajes;

public class SMensaje {
    public static final int REGISTRO = 0;
    public static final int NUMERO = 1;
    public static final int NUMEROS_ELEGIDOS = 2;
    public static final int INICIO = 3;
    public static final int MENSAJE = 4;
    public static final int DATOS = 5;
}
