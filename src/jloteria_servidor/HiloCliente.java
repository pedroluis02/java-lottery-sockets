
package jloteria_servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket; 
import mensajes.SMensaje;

public class HiloCliente extends Thread {
    public Socket socket;
    private DataInputStream flujoLectura;
    private DataOutputStream flujoEscritura;
    public String id;
    public String nombre;
    public boolean enJuego;
    public String numeroString;
    
    private VentanaServidor gui;

    public HiloCliente(Socket socket, int numero,VentanaServidor gui) {
        this.socket = socket;
        this.gui = gui;
        this.enJuego = false;
        this.numeroString = "";
        try {
            this.flujoLectura = new DataInputStream(socket.getInputStream());
            this.flujoEscritura = new DataOutputStream(socket.getOutputStream());
        } catch(IOException ex) {
            gui.mostraMensaje(ex.getMessage());
        }
    }
    @Override
    public void run() {
       try {
           while(true) {   
               int opcion = flujoLectura.readInt();
               switch(opcion) {
                   case SMensaje.DATOS:
                       nombre = flujoLectura.readUTF();
                       gui.mostraMensaje("Datos: id=" + id + " nombre=" + nombre);
                       break;
                   case SMensaje.NUMEROS_ELEGIDOS: 
                       numeroString = flujoLectura.readUTF();
                       enJuego = true;
                       gui.mostraMensaje("Numeros: id=" + id  + " cadena=" + numeroString);
                      
                       break;
               }
           }
       } catch(Exception ex) {
           gui.mostraMensaje(ex.getMessage());
       }       
       
       gui.mostraMensaje("Id[ " + id + "]: desconectado");
       VentanaServidor.numeroClientes--;
       gui.setNumeroClientes();
       VentanaServidor.clientes.remove(id);
       
    }
    public void enviarRegistroId(String idE) {
        try {
            id = idE;
            flujoEscritura.writeInt(SMensaje.REGISTRO);
            flujoEscritura.writeUTF(id);
            gui.mostraMensaje("Registro: nombre=" + nombre + " id=" + id);
        } catch(Exception ex) {
            gui.mostraMensaje(ex.getMessage());
        }
    }
    public void enviarInicioJuego(String mensaje) {
        try {
            flujoEscritura.writeInt(SMensaje.INICIO);
            flujoEscritura.writeUTF(mensaje);
        } catch(Exception ex) {
            gui.mostraMensaje(ex.getMessage());
        }
    }
    public void enviarMensaje(String mensaje) {
        try {
            flujoEscritura.writeInt(SMensaje.MENSAJE);
            flujoEscritura.writeUTF(mensaje);
        } catch(Exception ex) {
            gui.mostraMensaje(ex.getMessage());
        }
    }
    public void enviarNumero(int posicion, int numero) {
        if(posicion == 4) {
            enJuego = false;
        }
        try {
            flujoEscritura.writeInt(SMensaje.NUMERO);
            flujoEscritura.writeInt(posicion);
            flujoEscritura.writeInt(numero);
        } catch(Exception ex) {
            gui.mostraMensaje(ex.getMessage());
        }
    }
}
