
package jloteria_servidor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class VentanaServidor extends javax.swing.JFrame {

    public VentanaServidor() {
        initComponents();
        clientes = new HashMap<>();
        
        botonNroJugadores.removeMouseListener(
                botonNroJugadores.getMouseListeners()[0]);
        
        boton_0.removeMouseListener(boton_0.getMouseListeners()[0]);
        boton_1.removeMouseListener(boton_1.getMouseListeners()[0]);
        boton_2.removeMouseListener(boton_2.getMouseListeners()[0]);
        boton_3.removeMouseListener(boton_3.getMouseListeners()[0]);
        boton_4.removeMouseListener(boton_4.getMouseListeners()[0]);
        
        modelo = new DefaultListModel<>();
        listaGanadores.setModel(modelo);
        
        numeroClientes = 0;
        setNumeroClientes();
        
        tiempo = new Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                if(numeroPosicion == 5) {
                    tiempo.stop();
                    numeroPosicion = 0;
                    botonNumerosAzar.setEnabled(true);
                    String c = boton_0.getText() + boton_1.getText() + 
                            boton_2.getText() + boton_3.getText() + boton_4.getText();
                    buscarGandor(c);
                    return;
                }
                
                int n = numeroAlAzar();
                switch(numeroPosicion) {
                    case 0: boton_0.setText(n + "");
                        break;
                    case 1: boton_1.setText(n + "");
                        break;
                    case 2: boton_2.setText(n + "");
                        break;
                    case 3: boton_3.setText(n + "");
                        break;
                    case 4: boton_4.setText(n + "");
                        break;
                }
                enviarNumeroClientes(numeroPosicion, n);
                numeroPosicion++;
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        botonSalir = new javax.swing.JButton();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        boton_0 = new javax.swing.JButton();
        boton_1 = new javax.swing.JButton();
        boton_2 = new javax.swing.JButton();
        boton_3 = new javax.swing.JButton();
        boton_4 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        botonNumerosAzar = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        botonNroJugadores = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listaGanadores = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        areaTextoConsola = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);

        jPanel2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        botonSalir.setText("Salir");
        botonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSalirActionPerformed(evt);
            }
        });
        jPanel2.add(botonSalir);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Números Elegidos"));
        jPanel3.setOpaque(false);
        jPanel3.setLayout(new java.awt.GridLayout(1, 0, 10, 0));

        boton_0.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_0.setForeground(java.awt.Color.blue);
        boton_0.setText("?");
        jPanel3.add(boton_0);

        boton_1.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_1.setForeground(java.awt.Color.blue);
        boton_1.setText("?");
        jPanel3.add(boton_1);

        boton_2.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_2.setForeground(java.awt.Color.blue);
        boton_2.setText("?");
        jPanel3.add(boton_2);

        boton_3.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_3.setForeground(java.awt.Color.blue);
        boton_3.setText("?");
        jPanel3.add(boton_3);

        boton_4.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_4.setForeground(java.awt.Color.blue);
        boton_4.setText("?");
        jPanel3.add(boton_4);

        botonNumerosAzar.setText("Elegir Números al Azar");
        botonNumerosAzar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNumerosAzarActionPerformed(evt);
            }
        });
        jPanel4.add(botonNumerosAzar);

        jPanel5.setLayout(new java.awt.GridLayout());

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel1.setText("Número de Jugadores");
        jPanel5.add(jLabel1);

        botonNroJugadores.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        botonNroJugadores.setForeground(java.awt.Color.red);
        botonNroJugadores.setText("?");
        jPanel5.add(botonNroJugadores);
        jPanel5.add(jLabel2);

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Ganadores"));
        jPanel6.setLayout(new javax.swing.BoxLayout(jPanel6, javax.swing.BoxLayout.LINE_AXIS));

        listaGanadores.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(listaGanadores);

        jPanel6.add(jScrollPane1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 584, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane2.addTab("Datos de Juego", jPanel1);

        areaTextoConsola.setEditable(false);
        areaTextoConsola.setColumns(20);
        areaTextoConsola.setRows(5);
        jScrollPane2.setViewportView(areaTextoConsola);

        jTabbedPane2.addTab("Consola deEventos", jScrollPane2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTabbedPane2))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSalirActionPerformed
        // TODO add your handling code here:
        cerrarConexiones();
        System.exit(0);
    }//GEN-LAST:event_botonSalirActionPerformed

    private void botonNumerosAzarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNumerosAzarActionPerformed
        // TODO add your handling code here:
        if(clientes.isEmpty()) {
            JOptionPane.showMessageDialog(this, "No hay clientes Conectados");
            return;
        }
        if(analizarJugadores_Numeros() == false) {
            JOptionPane.showMessageDialog(this, "Ningun cliente ha enviado sus numeros");
            return;
        }
        botonNumerosAzar.setEnabled(false);
        tiempo.start();
    }//GEN-LAST:event_botonNumerosAzarActionPerformed

    private boolean analizarJugadores_Numeros() {
        for(String k : clientes.keySet()) {
            if(clientes.get(k).enJuego) {
                return true;
            }
        }
        return false;
    }
    
    private int numeroAlAzar() {
        Random r = new Random();
        return r.nextInt(LIMITE);
    }
    
    private void buscarGandor(String cadena) {
        boolean g = false;
        modelo.clear();
        for(String k : clientes.keySet()) {
            if(clientes.get(k).numeroString.compareTo(cadena) == 0) {
                modelo.addElement(clientes.get(k).nombre + ": " + k);
                g = true; 
            }
        }
        if(g == false) {
            JOptionPane.showMessageDialog(this, "Ningun Ganador");
        }
    }
    
    private void enviarNumeroClientes(int posicion, int numero) {
        for(String k : clientes.keySet()) {
            if(clientes.get(k).enJuego) {
                clientes.get(k).enviarNumero(posicion, numero);
            }
        }
    }
    
    public void setNumeroClientes() {
        botonNroJugadores.setText(numeroClientes + "");
    }
    public void iniciarServidor() {
        try {
            serverSocket = new ServerSocket(PUERTO);
            mostraMensaje(new Date().toGMTString()
                    + ": servidor iniciado puerto=" + PUERTO);
            setTitle(getTitle() + " - Puerto[" + PUERTO + "]");
            while(true) {
                Socket socket = serverSocket.accept();
                HiloCliente hc = new HiloCliente(socket, clientes.size(), this);
                hc.start();
                
                String id = new Date().getTime() + "";
                clientes.put(id, hc);
                
                hc.enviarRegistroId(id);
                numeroClientes++;
                
                botonNroJugadores.setText(numeroClientes + "");
            }
        } catch(Exception ex) {
            mostraMensaje(ex.getMessage());
        }
    }
    
    public void mostraMensaje(String mensaje) {
        areaTextoConsola.append("\n" + mensaje);
        System.out.println(mensaje);
    }
    
    private void cerrarConexiones() {
        for(String k : clientes.keySet()) {
            clientes.get(k).stop();
            try {
                clientes.get(k).socket.close();
            } catch(Exception ex) {
                mostraMensaje(ex.getMessage());
            }
        }
        clientes.clear();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaTextoConsola;
    private javax.swing.JButton botonNroJugadores;
    private javax.swing.JButton botonNumerosAzar;
    private javax.swing.JButton botonSalir;
    private javax.swing.JButton boton_0;
    private javax.swing.JButton boton_1;
    private javax.swing.JButton boton_2;
    private javax.swing.JButton boton_3;
    private javax.swing.JButton boton_4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JList listaGanadores;
    // End of variables declaration//GEN-END:variables
    private ServerSocket serverSocket;
    public static HashMap <String, HiloCliente> clientes;
    private int PUERTO = 5000;
    public static int numeroClientes;
    //
    private Timer tiempo;
    private int numeroPosicion = 0;
    private int LIMITE = 4;
    private DefaultListModel <String> modelo;
}
