
package jloteria_cliente;

import java.awt.HeadlessException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import mensajes.SMensaje;

public class HiloConexion extends Thread {
    private Socket socket;
    private DataInputStream flujoLectura;
    private DataOutputStream flujoEscritura;
    //
    public String id;
    public String nombre;
    public String numeroCadena;
    
    private VentaCliente gui;

    public HiloConexion(Socket socket, String nombre, VentaCliente gui) {
        this.socket = socket;
        this.gui = gui;
        this.nombre = nombre;
        try {
            this.flujoLectura = new DataInputStream(socket.getInputStream());
            this.flujoEscritura = new DataOutputStream(socket.getOutputStream());
        } catch(Exception ex) {
            gui.mostrarMensajeBarra(ex.getMessage());
        }
    }
    @Override
    public void run() {
        try {
            while(true) {
                int opcion = flujoLectura.readInt();
                switch(opcion) {
                    case SMensaje.REGISTRO:
                        id = flujoLectura.readUTF();
                        gui.cambioConectado();
                        gui.mostrarMensajeBarra("Te has conectado:  id=" + id);
                        enviarDatos();
                        break;
                    case SMensaje.INICIO:
                        String mm = flujoLectura.readUTF();
                        gui.mostrarMensaje(mm);
                        break;
                    case SMensaje.MENSAJE:
                        String m = flujoLectura.readUTF();
                        gui.mostrarMensaje(m);
                        break;
                    case SMensaje.NUMERO:
                        int pos = flujoLectura.readInt(),
                            n   = flujoLectura.readInt();
                        gui.mostrarMensajeBarra("Número: pos=" + pos + " n="+ n);
                        gui.colocarNumeroGanador(pos, n);
                        // ejecuatar accion
                        break;
                }
            }
        } catch(IOException ex)  {
            gui.mostrarMensajeBarra(ex.getMessage());
        }
        
        gui.cambioGuiDesconectado();
        gui.mostrarMensajeBarra("Conexión con Servidor: perdida");
    }
    //
    public String enviarDatos() {
        try {
            flujoEscritura.writeInt(SMensaje.DATOS);
            flujoEscritura.writeUTF(nombre);
        } catch(Exception ex) {
            return ex.getMessage();
        }
        return "";
    }
    public String enviarNumerosElegidos(String numeroString) {
        numeroCadena = numeroString;
        try {
            flujoEscritura.writeInt(SMensaje.NUMEROS_ELEGIDOS);
            flujoEscritura.writeUTF(numeroString);
        } catch(Exception ex) {
            return ex.getMessage();
        }
        return "";
    }
}
