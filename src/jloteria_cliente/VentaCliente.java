
package jloteria_cliente;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class VentaCliente extends JFrame implements ActionListener {

    public VentaCliente() {
        initComponents();
        
        boton_0.removeMouseListener(boton_0.getMouseListeners()[0]);
        boton_1.removeMouseListener(boton_1.getMouseListeners()[0]);
        boton_2.removeMouseListener(boton_2.getMouseListeners()[0]);
        boton_3.removeMouseListener(boton_3.getMouseListeners()[0]);
        boton_4.removeMouseListener(boton_4.getMouseListeners()[0]);
        
        //
        tiempoMensaje = new Timer(5000, this);
        tiempoMensaje.setActionCommand("tiempo");
        String ip, h;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
            h = InetAddress.getLocalHost().getHostName();
        } catch(Exception ex) {
            ip = "localhost";
            h  = "";
        }
        textoJugador.setText(h);
        textoDirIP.setText(ip);
        textoPuerto.setText(PUERTO + "");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        textoJugador = new javax.swing.JTextField();
        textoDirIP = new javax.swing.JTextField();
        textoPuerto = new javax.swing.JTextField();
        botonConectar = new javax.swing.JButton();
        botonDesconectar = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jToolBar2 = new javax.swing.JToolBar();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        textoMensaje = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        boton_0_J = new javax.swing.JButton();
        boton_1_J = new javax.swing.JButton();
        boton_2_J = new javax.swing.JButton();
        boton_3_J = new javax.swing.JButton();
        boton_4_J = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        boton_0 = new javax.swing.JButton();
        boton_1 = new javax.swing.JButton();
        boton_2 = new javax.swing.JButton();
        boton_3 = new javax.swing.JButton();
        boton_4 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        botonEnviarNumeros = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Configuración[Nombre-IP-Puerto]"));
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.LINE_AXIS));

        textoJugador.setToolTipText("Nombre de Jugador");
        jPanel1.add(textoJugador);

        textoDirIP.setToolTipText("Dirección IP del Servidor");
        jPanel1.add(textoDirIP);

        textoPuerto.setToolTipText("Puerto de conexion");
        jPanel1.add(textoPuerto);

        botonConectar.setText("Conectar");
        botonConectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonConectarActionPerformed(evt);
            }
        });
        jPanel1.add(botonConectar);

        botonDesconectar.setText("Desconectar");
        botonDesconectar.setEnabled(false);
        botonDesconectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonDesconectarActionPerformed(evt);
            }
        });
        jPanel1.add(botonDesconectar);
        jPanel1.add(jSeparator2);

        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);
        jToolBar2.add(jSeparator1);

        textoMensaje.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jToolBar2.add(textoMensaje);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Números Elegidos"));
        jPanel3.setOpaque(false);
        jPanel3.setLayout(new java.awt.GridLayout(1, 0, 10, 0));

        boton_0_J.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_0_J.setForeground(new java.awt.Color(40, 117, 40));
        boton_0_J.setText("?");
        boton_0_J.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_0_JActionPerformed(evt);
            }
        });
        jPanel3.add(boton_0_J);

        boton_1_J.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_1_J.setForeground(new java.awt.Color(40, 117, 40));
        boton_1_J.setText("?");
        boton_1_J.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_1_JActionPerformed(evt);
            }
        });
        jPanel3.add(boton_1_J);

        boton_2_J.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_2_J.setForeground(new java.awt.Color(40, 117, 40));
        boton_2_J.setText("?");
        boton_2_J.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_2_JActionPerformed(evt);
            }
        });
        jPanel3.add(boton_2_J);

        boton_3_J.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_3_J.setForeground(new java.awt.Color(40, 117, 40));
        boton_3_J.setText("?");
        boton_3_J.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_3_JActionPerformed(evt);
            }
        });
        jPanel3.add(boton_3_J);

        boton_4_J.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_4_J.setForeground(new java.awt.Color(40, 117, 40));
        boton_4_J.setText("?");
        boton_4_J.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_4_JActionPerformed(evt);
            }
        });
        jPanel3.add(boton_4_J);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Números Ganadores"));
        jPanel4.setOpaque(false);
        jPanel4.setLayout(new java.awt.GridLayout(1, 0, 10, 0));

        boton_0.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_0.setForeground(java.awt.Color.blue);
        boton_0.setText("?");
        jPanel4.add(boton_0);

        boton_1.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_1.setForeground(java.awt.Color.blue);
        boton_1.setText("?");
        jPanel4.add(boton_1);

        boton_2.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_2.setForeground(java.awt.Color.blue);
        boton_2.setText("?");
        jPanel4.add(boton_2);

        boton_3.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_3.setForeground(java.awt.Color.blue);
        boton_3.setText("?");
        jPanel4.add(boton_3);

        boton_4.setFont(new java.awt.Font("Ubuntu", 1, 36)); // NOI18N
        boton_4.setForeground(java.awt.Color.blue);
        boton_4.setText("?");
        jPanel4.add(boton_4);

        botonEnviarNumeros.setText("Enviar Números Elegidos");
        botonEnviarNumeros.setEnabled(false);
        botonEnviarNumeros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEnviarNumerosActionPerformed(evt);
            }
        });
        jPanel5.add(botonEnviarNumeros);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 525, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(148, 148, 148)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(22, 22, 22)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(218, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, 577, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 553, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private String conectar(String host, int puerto) {
        try {
            socket = new Socket(host, puerto);
            hc = new HiloConexion(socket, textoJugador.getText(), this);
            hc.start();
            
        } catch(Exception ex) {
            return ex.getMessage();
        }
        return "";
    }
    private String desconectar() {
        try {
            hc.stop();
            socket.close();
        } catch(Exception ex) {
            return ex.getMessage();
        }
        return "";
    }
    public void cambioGuiDesconectado() {
        botonConectar.setEnabled(true);
        botonDesconectar.setEnabled(false);
        botonEnviarNumeros.setEnabled(false);
        setTitle("Venta Cliente - Desconectado");
        cadenaRecibida = "";
    }
    
    public void cambioConectado() {
        botonConectar.setEnabled(false);
        botonDesconectar.setEnabled(true);
        botonEnviarNumeros.setEnabled(true);
        setTitle("Venta Cliente - Id[" + hc.id + "]");
    }
    
    private void botonConectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonConectarActionPerformed
        // TODO add your handling code here:
        String co = conectar(textoDirIP.getText(), PUERTO);
        if(co.isEmpty()) {
            cambioConectado();
        } else {
            mostrarMensajeBarra(co);
        }
    }//GEN-LAST:event_botonConectarActionPerformed

    private void botonDesconectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonDesconectarActionPerformed
        // TODO add your handling code here:
        String m = desconectar();
        if(m.isEmpty()) {
            mostrarMensajeBarra("Te has desconectado");
            cambioGuiDesconectado();
        } else {
            mostrarMensajeBarra(m);
        }
    }//GEN-LAST:event_botonDesconectarActionPerformed

    private int elegirNumeroAzar() {
        Random r = new Random();
        return r.nextInt(LIMITE);
    }
    
    private void boton_0_JActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_0_JActionPerformed
        // TODO add your handling code here:
        boton_0_J.setText(elegirNumeroAzar() + "");
    }//GEN-LAST:event_boton_0_JActionPerformed

    private void boton_1_JActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_1_JActionPerformed
        // TODO add your handling code here:
        boton_1_J.setText(elegirNumeroAzar() + "");
    }//GEN-LAST:event_boton_1_JActionPerformed

    private void boton_2_JActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_2_JActionPerformed
        // TODO add your handling code here:
        boton_2_J.setText(elegirNumeroAzar() + "");
    }//GEN-LAST:event_boton_2_JActionPerformed

    private void boton_3_JActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_3_JActionPerformed
        // TODO add your handling code here:
        boton_3_J.setText(elegirNumeroAzar() + "");
    }//GEN-LAST:event_boton_3_JActionPerformed

    private void boton_4_JActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_4_JActionPerformed
        // TODO add your handling code here:
        boton_4_J.setText(elegirNumeroAzar() + "");
    }//GEN-LAST:event_boton_4_JActionPerformed

    private void botonEnviarNumerosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEnviarNumerosActionPerformed
        // TODO add your handling code here:
        if(boton_0_J.getText().compareTo("?") == 0) {
            mostrarMensaje("Seleccione numero");
            return ;
        }
        if(boton_1_J.getText().compareTo("?") == 0) {
            mostrarMensaje("Seleccione numero");
            return ;
        }
        if(boton_2_J.getText().compareTo("?") == 0) {
            mostrarMensaje("Seleccione numero");
            return ;
        }
        if(boton_3_J.getText().compareTo("?") == 0) {
            mostrarMensaje("Seleccione numero");
            return ;
        }
        
        String numeroString = boton_0_J.getText() + boton_1_J.getText() +
                boton_2_J.getText() + boton_3_J.getText() + boton_4_J.getText();
        
        hc.enviarNumerosElegidos(numeroString);
        botonEnviarNumeros.setEnabled(false);
    }//GEN-LAST:event_botonEnviarNumerosActionPerformed

    public void colocarNumeroGanador(int posicion , int numero) {
        switch(posicion) {
            case 0: boton_0.setText(numero + "");
                break;
            case 1: boton_1.setText(numero + "");
                break;
            case 2: boton_2.setText(numero + "");
                break;
            case 3: boton_3.setText(numero + "");
                break;
            case 4: boton_4.setText(numero + "");
                break;
        }
        cadenaRecibida += numero + ""; 
        if(posicion == 4) {
            if(cadenaRecibida.compareTo(hc.numeroCadena) == 0) {
                mostrarMensaje("Has Ganado");
            } else {
                mostrarMensaje("Has Perdido");
            }
            botonEnviarNumeros.setEnabled(true);
            cadenaRecibida = "";
        }
    }
    
    public void mostrarMensajeBarra(String mensaje) {
        tiempoMensaje.stop();
        textoMensaje.setText("");
        tiempoMensaje.start();
        textoMensaje.setText(mensaje);
        System.out.println(mensaje);
    }
    public void mostrarMensaje(String mensaje) {
        JOptionPane.showMessageDialog(this, mensaje);
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getActionCommand().compareTo("tiempo") == 0) {
            textoMensaje.setText("");
            tiempoMensaje.stop();
        }
    }
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonConectar;
    private javax.swing.JButton botonDesconectar;
    private javax.swing.JButton botonEnviarNumeros;
    private javax.swing.JButton boton_0;
    private javax.swing.JButton boton_0_J;
    private javax.swing.JButton boton_1;
    private javax.swing.JButton boton_1_J;
    private javax.swing.JButton boton_2;
    private javax.swing.JButton boton_2_J;
    private javax.swing.JButton boton_3;
    private javax.swing.JButton boton_3_J;
    private javax.swing.JButton boton_4;
    private javax.swing.JButton boton_4_J;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JTextField textoDirIP;
    private javax.swing.JTextField textoJugador;
    private javax.swing.JLabel textoMensaje;
    private javax.swing.JTextField textoPuerto;
    // End of variables declaration//GEN-END:variables
    private int PUERTO = 5000;
    private Socket socket;
    private HiloConexion hc;
    //
    private Timer tiempoMensaje;
    private int LIMITE = 4;
    //
    private String cadenaRecibida = "";
}
